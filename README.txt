###
	Instruções para deploy showmethecode

#	Pré-Requisitos

1 - Instância Linux AWS com docker instalado e permissão correta no arquivo /var/run/docker.sock

2 - Notebook com Git instalado e conta no GitLab com Runner, e ambiente staging para rodar o pipeline

1 - Clonar o projeto iac do git:
git clone https://github.com/jmuniz1985/iac.git

2 - No arquivo .gitlab-ci.yml altere todos FQDNs para o seu respectivo 

3 - Sobrescreva o arquivo infracode.pem com o seu respectivo 

4 - Comite o projeto

5 - Acesse a aplicação

Este projeto está live em 
http://ec2-3-15-184-19.us-east-2.compute.amazonaws.com:3000/

A pasta build/libs é onde você carrega o .jar que vai ser feito deploy


